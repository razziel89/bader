!-----------------------------------------------------------------------------------!
! Bader charge density analysis program
!  Module for reading and writing charge density data
!-----------------------------------------------------------------------------------!

MODULE xyzdx_mod
  USE kind_mod
  USE matrix_mod
  USE ions_mod
  USE charge_mod
  USE options_mod
  IMPLICIT NONE

  PRIVATE
  PUBLIC :: read_charge_xyzdx, get_atomic_label, get_atomic_num, is_numeric

  CONTAINS

  FUNCTION is_numeric(string)
    IMPLICIT NONE
    CHARACTER(len=*), INTENT(IN) :: string
    LOGICAL :: is_numeric
    REAL :: x
    INTEGER :: e
    READ(string,*,IOSTAT=e) x
    is_numeric = e == 0
  END FUNCTION is_numeric

!-----------------------------------------------------------------------------------!
! read_charge_cube: Reads the charge density from a file in vasp or Gaussian cube 
!   format, by first reading the header, and then charges
!-----------------------------------------------------------------------------------!

  INTEGER FUNCTION get_atomic_num(element)
    IMPLICIT NONE
    CHARACTER(LEN=4), INTENT(IN) :: element
    CHARACTER(LEN=4) :: temp_element
    INTEGER :: el_nr, it_element
    temp_element=ADJUSTL(element)
    it_element=LEN_TRIM(temp_element)
    IF ( is_numeric(temp_element(1:it_element)) ) THEN
        READ (temp_element, *) get_atomic_num
    ELSE
        SELECT CASE (element(1:2))
        CASE ("H ")
            el_nr=1
        CASE ("He")
            el_nr=2
        CASE ("Li")
            el_nr=3
        CASE ("Be")
            el_nr=4
        CASE ("B ")
            el_nr=5
        CASE ("C ")
            el_nr=6
        CASE ("N ")
            el_nr=7
        CASE ("O ")
            el_nr=8
        CASE ("F ")
            el_nr=9
        CASE ("Ne")
            el_nr=10
        CASE ("Na")
            el_nr=11
        CASE ("Mg")
            el_nr=12
        CASE ("Al")
            el_nr=13
        CASE ("Si")
            el_nr=14
        CASE ("P ")
            el_nr=15
        CASE ("S ")
            el_nr=16
        CASE ("Cl")
            el_nr=17
        CASE ("Ar")
            el_nr=18
        CASE ("K ")
            el_nr=19
        CASE ("Ca")
            el_nr=20
        CASE ("Sc")
            el_nr=21
        CASE ("Ti")
            el_nr=22
        CASE ("V ")
            el_nr=23
        CASE ("Cr")
            el_nr=24
        CASE ("Mn")
            el_nr=25
        CASE ("Fe")
            el_nr=26
        CASE ("Co")
            el_nr=27
        CASE ("Ni")
            el_nr=28
        CASE ("Cu")
            el_nr=29
        CASE ("Zn")
            el_nr=30
        CASE ("Ga")
            el_nr=31
        CASE ("Ge")
            el_nr=32
        CASE ("As")
            el_nr=33
        CASE ("Se")
            el_nr=34
        CASE ("Br")
            el_nr=35
        CASE ("Kr")
            el_nr=36
        CASE ("Rb")
            el_nr=37
        CASE ("Sr")
            el_nr=38
        CASE ("Y ")
            el_nr=39
        CASE ("Zr")
            el_nr=40
        CASE ("Nb")
            el_nr=41
        CASE ("Mo")
            el_nr=42
        CASE ("Tc")
            el_nr=43
        CASE ("Ru")
            el_nr=44
        CASE ("Rh")
            el_nr=45
        CASE ("Pd")
            el_nr=46
        CASE ("Ag")
            el_nr=47
        CASE ("Cd")
            el_nr=48
        CASE ("In")
            el_nr=49
        CASE ("Sn")
            el_nr=50
        CASE ("Sb")
            el_nr=51
        CASE ("Te")
            el_nr=52
        CASE ("I")
            el_nr=53
        CASE ("Xe")
            el_nr=54
        CASE ("Cs")
            el_nr=55
        CASE ("Ba")
            el_nr=56
        CASE ("La")
            el_nr=57
        CASE ("Ce")
            el_nr=58
        CASE ("Pr")
            el_nr=59
        CASE ("Nd")
            el_nr=60
        CASE ("Pm")
            el_nr=61
        CASE ("Sm")
            el_nr=62
        CASE ("Eu")
            el_nr=63
        CASE ("Gd")
            el_nr=64
        CASE ("Tb")
            el_nr=65
        CASE ("Dy")
            el_nr=66
        CASE ("Ho")
            el_nr=67
        CASE ("Er")
            el_nr=68
        CASE ("Tm")
            el_nr=69
        CASE ("Yb")
            el_nr=70
        CASE ("Lu")
            el_nr=71
        CASE ("Hf")
            el_nr=72
        CASE ("Ta")
            el_nr=73
        CASE ("W")
            el_nr=74
        CASE ("Re")
            el_nr=75
        CASE ("Os")
            el_nr=76
        CASE ("Ir")
            el_nr=77
        CASE ("Pt")
            el_nr=78
        CASE ("Au")
            el_nr=79
        CASE ("Hg")
            el_nr=80
        CASE ("Tl")
            el_nr=81
        CASE ("Pb")
            el_nr=82
        CASE ("Bi")
            el_nr=83
        CASE ("Po")
            el_nr=84
        CASE ("At")
            el_nr=85
        CASE ("Rn")
            el_nr=86
        CASE ("Fr")
            el_nr=87
        CASE ("Ra")
            el_nr=88
        CASE ("Ac")
            el_nr=89
        CASE ("Th")
            el_nr=90
        CASE ("Pa")
            el_nr=91
        CASE ("U")
            el_nr=92
        CASE ("Np")
            el_nr=93
        CASE ("Pu")
            el_nr=94
        CASE ("Am")
            el_nr=95
        CASE ("Cm")
            el_nr=96
        CASE ("Bk")
            el_nr=97
        CASE ("Cf")
            el_nr=98
        CASE ("Es")
            el_nr=99
        CASE ("Fm")
            el_nr=100
        CASE ("Md")
            el_nr=101
        CASE ("No")
            el_nr=102
        CASE ("Lr")
            el_nr=103
        CASE ("Rf")
            el_nr=104
        CASE ("Db")
            el_nr=105
        CASE ("Sg")
            el_nr=106
        CASE ("Bh")
            el_nr=107
        CASE ("Hs")
            el_nr=108
        CASE ("Mt")
            el_nr=109
        CASE ("Ds")
            el_nr=110
        CASE ("Rg")
            el_nr=111
        CASE ("Cn")
            el_nr=112
        CASE ("Uut")
            el_nr=113
        CASE ("Fl")
            el_nr=114
        CASE ("Uup")
            el_nr=115
        CASE ("Lv")
            el_nr=116
        CASE DEFAULT
            el_nr=0
        END SELECT
        get_atomic_num = el_nr
    END IF
    RETURN
  END FUNCTION get_atomic_num

  CHARACTER(LEN=4) FUNCTION get_atomic_label(num)
    IMPLICIT NONE
    CHARACTER(LEN=4) :: element
    INTEGER, INTENT(IN):: num
    SELECT CASE (num)
    CASE (1)
        element="H "
    CASE (2)
        element="He"
    CASE (3)
        element="Li"
    CASE (4)
        element="Be"
    CASE (5)
        element="B "
    CASE (6)
        element="C "
    CASE (7)
        element="N "
    CASE (8)
        element="O "
    CASE (9)
        element="F "
    CASE (10)
        element="Ne"
    CASE (11)
        element="Na"
    CASE (12)
        element="Mg"
    CASE (13)
        element="Al"
    CASE (14)
        element="Si"
    CASE (15)
        element="P "
    CASE (16)
        element="S "
    CASE (17)
        element="Cl"
    CASE (18)
        element="Ar"
    CASE (19)
        element="K "
    CASE (20)
        element="Ca"
    CASE (21)
        element="Sc"
    CASE (22)
        element="Ti"
    CASE (23)
        element="V "
    CASE (24)
        element="Cr"
    CASE (25)
        element="Mn"
    CASE (26)
        element="Fe"
    CASE (27)
        element="Co"
    CASE (28)
        element="Ni"
    CASE (29)
        element="Cu"
    CASE (30)
        element="Zn"
    CASE (31)
        element="Ga"
    CASE (32)
        element="Ge"
    CASE (33)
        element="As"
    CASE (34)
        element="Se"
    CASE (35)
        element="Br"
    CASE (36)
        element="Kr"
    CASE (37)
        element="Rb"
    CASE (38)
        element="Sr"
    CASE (39)
        element="Y "
    CASE (40)
        element="Zr"
    CASE (41)
        element="Nb"
    CASE (42)
        element="Mo"
    CASE (43)
        element="Tc"
    CASE (44)
        element="Ru"
    CASE (45)
        element="Rh"
    CASE (46)
        element="Pd"
    CASE (47)
        element="Ag"
    CASE (48)
        element="Cd"
    CASE (49)
        element="In"
    CASE (50)
        element="Sn"
    CASE (51)
        element="Sb"
    CASE (52)
        element="Te"
    CASE (53)
        element="I"
    CASE (54)
        element="Xe"
    CASE (55)
        element="Cs"
    CASE (56)
        element="Ba"
    CASE (57)
        element="La"
    CASE (58)
        element="Ce"
    CASE (59)
        element="Pr"
    CASE (60)
        element="Nd"
    CASE (61)
        element="Pm"
    CASE (62)
        element="Sm"
    CASE (63)
        element="Eu"
    CASE (64)
        element="Gd"
    CASE (65)
        element="Tb"
    CASE (66)
        element="Dy"
    CASE (67)
        element="Ho"
    CASE (68)
        element="Er"
    CASE (69)
        element="Tm"
    CASE (70)
        element="Yb"
    CASE (71)
        element="Lu"
    CASE (72)
        element="Hf"
    CASE (73)
        element="Ta"
    CASE (74)
        element="W"
    CASE (75)
        element="Re"
    CASE (76)
        element="Os"
    CASE (77)
        element="Ir"
    CASE (78)
        element="Pt"
    CASE (79)
        element="Au"
    CASE (80)
        element="Hg"
    CASE (81)
        element="Tl"
    CASE (82)
        element="Pb"
    CASE (83)
        element="Bi"
    CASE (84)
        element="Po"
    CASE (85)
        element="At"
    CASE (86)
        element="Rn"
    CASE (87)
        element="Fr"
    CASE (88)
        element="Ra"
    CASE (89)
        element="Ac"
    CASE (90)
        element="Th"
    CASE (91)
        element="Pa"
    CASE (92)
        element="U"
    CASE (93)
        element="Np"
    CASE (94)
        element="Pu"
    CASE (95)
        element="Am"
    CASE (96)
        element="Cm"
    CASE (97)
        element="Bk"
    CASE (98)
        element="Cf"
    CASE (99)
        element="Es"
    CASE (100)
        element="Fm"
    CASE (101)
        element="Md"
    CASE (102)
        element="No"
    CASE (103)
        element="Lr"
    CASE (104)
        element="Rf"
    CASE (105)
        element="Db"
    CASE (106)
        element="Sg"
    CASE (107)
        element="Bh"
    CASE (108)
        element="Hs"
    CASE (109)
        element="Mt"
    CASE (110)
        element="Ds"
    CASE (111)
        element="Rg"
    CASE (112)
        element="Cn"
    CASE (113)
        element="Uut"
    CASE (114)
        element="Fl"
    CASE (115)
        element="Uup"
    CASE (116)
        element="Lv"
    CASE DEFAULT
        element="XXX"
    END SELECT
    get_atomic_label = element
    RETURN
  END FUNCTION get_atomic_label

  SUBROUTINE read_charge_xyzdx(ions,chg,chargefile,opts)

    TYPE(ions_obj) :: ions
    TYPE(charge_obj) :: chg
    CHARACTER(LEN=128) :: chargefile
    TYPE(options_obj) :: opts

    REAL(q2) :: vol
    REAL(q2), DIMENSION(3) :: dlat, dcar
    INTEGER :: i, n1, n2, n3, d1, d2, d3
    REAL(q2) :: total_charge

    CHARACTER(len=1000) :: buff 
    CHARACTER(len=100) :: sbuff 
    CHARACTER(len=20) :: disc1, disc2, disc3, disc4, disc5
    INTEGER :: it, r1, r2, r3, r4, r5

    OPEN(100,FILE=chargefile(1:LEN_TRIM(ADJUSTL(chargefile))), &
    &    STATUS='old',ACTION='read')
    WRITE(*,'(/,1A11,1A20)') 'OPEN ... ',chargefile
    WRITE(*,'(1A27)') 'XYZDX-STYLE INPUT FILE'

    READ(100,*) ions%nions
    !Skip the second line
    READ(100,'()') 

    !READ(100,*) ions%nions, chg%org_car

    ALLOCATE(ions%r_car(ions%nions,3))
    ALLOCATE(ions%r_dir(ions%nions,3))
    ALLOCATE(ions%ion_chg(ions%nions))
    ALLOCATE(ions%atomic_num(ions%nions))
    ALLOCATE(ions%atomic_name(ions%nions))

    DO i = 1, ions%nions
      READ(100,*) ions%atomic_name(i),ions%r_car(i,:)
      ions%r_car(i,:) = 1.889725989 * ions%r_car(i,:)
      ions%ion_chg(i) = 0._q2
      ions%atomic_num(i) = get_atomic_num(ions%atomic_name(i))
      total_charge = total_charge + REAL(ions%atomic_num(i),q2)
      !ions%r_dir(i,:) = MATMUL(ions%car2dir,ions%r_car(i,:)-chg%org_car(:))
    END DO

    READ(100,"(A)") buff
    sbuff = ADJUSTL(buff)
    it = LEN_TRIM(sbuff)
    DO WHILE(sbuff(1:1) == "#")
        READ(100,"(A)") buff
        sbuff = ADJUSTL(buff)
        it = LEN_TRIM(sbuff)
    ENDDO
    
    READ(buff,*) disc1, r1, disc2, disc3, disc4, chg%npts
    disc1 = ADJUSTL(disc1)
    disc2 = ADJUSTL(disc2)
    disc3 = ADJUSTL(disc3)
    disc4 = ADJUSTL(disc4)
    chg%i_npts = 1._q2/REAL(chg%npts,q2)
    IF (disc1 .NE. "object" .OR. disc2 .NE. "class" .OR. disc3 .NE. "gridpositions" .OR. disc4 .NE. "counts") THEN
        STOP "WRONG FORMAT OF FIRST LINE IN DX PART"
    ENDIF
    READ(100,*) disc1, chg%org_car
    chg%org_car = 1.889725989 * chg%org_car
    IF (disc1 .NE. "origin") THEN
        STOP "WRONG FORMAT OF SECOND LINE IN DX PART"
    ENDIF
    DO i=1,3
        READ(100,*) disc1, ions%lattice(i,1:3)
        IF (disc1 .NE. "delta") THEN
            STOP "WRONG FORMAT IN VOXEL SIZE DECLARATION IN DX PART"
        ENDIF
    ENDDO
    ions%lattice = 1.889725989 * ions%lattice
    !skip next lines
    READ(100,"(/)")

    chg%lat2car = TRANSPOSE(ions%lattice)
    DO i=1,3
      ions%lattice(i,:) = ions%lattice(i,:)*REAL(chg%npts(i),q2)
    END DO
    ions%dir2car = TRANSPOSE(ions%lattice)
    ions%car2dir = inverse(ions%dir2car)
    chg%car2lat = inverse(chg%lat2car)
    vol = matrix_volume(ions%lattice)

    DO i = 1, ions%nions
      ions%r_dir(i,:) = MATMUL(ions%car2dir,ions%r_car(i,:)-chg%org_car(:))
    END DO


    ! Note: this is only for isolated atoms.  For periodic systems, this shift
    ! might not be appropriate

    ! origin of the lattice is at chg(0.5,0.5,0.5)
!    chg%org_lat=(/0.5_q2,0.5_q2,0.5_q2/)
    chg%org_lat = (/1._q2,1._q2,1._q2/)
!    CALL matrix_vector(ions%car2dir,chg%org_car,chg%org_dir)

    ALLOCATE(ions%r_lat(ions%nions,3))
    DO i=1,ions%nions
!      ions%r_lat(i,:)=dir2lat(chg,ions%r_dir(i,:))
      ions%r_lat(i,:) = MATMUL(chg%car2lat, ions%r_car(i,:) - chg%org_car(:))
      ions%r_lat(i,:) = ions%r_lat(i,:) + chg%org_lat
      CALL pbc_r_lat(ions%r_lat(i,:),chg%npts)
    END DO

    chg%nrho = PRODUCT(chg%npts(:))
    ALLOCATE(chg%rho(chg%npts(1),chg%npts(2),chg%npts(3)))
    READ(100,*) (((chg%rho(n1,n2,n3),  &
    &            n3 = 1,chg%npts(3)), n2 = 1,chg%npts(2)),n1 = 1,chg%npts(1))

    IF (opts%adjust_charges .NE. 0) THEN
        IF (opts%adjust_charges .GE. 10) THEN
            chg%rho=total_charge/SUM(chg%rho)*chg%rho
        ENDIF
        IF (MOD(opts%adjust_charges,10) == 1) THEN
            chg%rho=chg%rho/SUM(chg%rho)*opts%total_charge
        ELSE IF (MOD(opts%adjust_charges,10) == 2) THEN
            chg%rho=chg%rho+(opts%total_charge/SUM(chg%rho))*chg%rho
        ENDIF
    ENDIF
    chg%rho=chg%npts(1)*chg%npts(2)*chg%npts(3)*chg%rho
    !DO n1 = 1,chg%npts(1)
    !  DO n2 = 1,chg%npts(2)
    !    DO n3 = 1,chg%npts(3)
    !      !chg%rho(n1,n2,n3) = vol*chg%rho(n1,n2,n3)
    !      chg%rho(n1,n2,n3) = chg%npts(1)*chg%npts(2)*chg%npts(3)*chg%rho(n1,n2,n3)
    !    END DO
    !  END DO
    !END DO

    WRITE(*,'(1A12,1I5,1A2,1I4,1A2,1I4)') 'FFT-grid: ',  &
    &         chg%npts(1),'x',chg%npts(2),'x',chg%npts(3)
    WRITE(*,'(2x,A,1A20)') 'CLOSE ... ', chargefile
    CLOSE(100)

    ! distance between neighboring points
    DO d1 = -1, 1
      dlat(1) = REAL(d1,q2)
      DO d2 = -1, 1
        dlat(2) = REAL(d2,q2)
        DO d3 = -1, 1
          dlat(3) = REAL(d3,q2)
          dcar = MATMUL(chg%lat2car, dlat)
          chg%lat_dist(d1,d2,d3) = SQRT(SUM(dcar*dcar))
         IF ((d1 == 0).AND.(d2 == 0).AND.(d3 == 0)) THEN
            chg%lat_i_dist(d1,d2,d3) = 0._q2
          ELSE
            chg%lat_i_dist(d1,d2,d3) = 1._q2/chg%lat_dist(d1,d2,d3)
          END IF
        END DO
      END DO
    END DO
 
    !add ions%num_ion as the total atom number in case some want to write 
    !a chgcar file from a cube file
!    ALLOCATE(ions%num_ion(1))
!    ions%num_ion(1)=ions%nions

  RETURN
  END SUBROUTINE read_charge_xyzdx

END MODULE xyzdx_mod

